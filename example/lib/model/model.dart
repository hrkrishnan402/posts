class PostsList {
    PostsList({
        this.data,
    });

    final List<Post>? data;

    PostsList copyWith({
        List<Post>? data,
    }) => 
        PostsList(
            data: data ?? this.data,
        );

    factory PostsList.fromJson(List json) => PostsList(
        data: List<Post>.from(json.map((x) => Post.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from((data ?? []).map((x) => x.toJson())),
    };
}

class Post {
    Post({
        this.userId,
        this.id,
        this.title,
        this.body,
    });

    final int? userId;
    final int? id;
    final String? title;
    final String? body;

    Post copyWith({
        int? userId,
        int? id,
        String? title,
        String? body,
    }) => 
        Post(
            userId: userId ?? this.userId,
            id: id ?? this.id,
            title: title ?? this.title,
            body: body ?? this.body,
        );

    factory Post.fromJson(Map<String, dynamic> json) => Post(
        userId: json["userId"],
        id: json["id"],
        title: json["title"],
        body: json["body"],
    );

    Map<String, dynamic> toJson() => {
        "userId": userId,
        "id": id,
        "title": title,
        "body": body,
    };
}
