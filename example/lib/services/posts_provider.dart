import 'package:dio/dio.dart';
import 'package:example/model/model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class PostsProvider with ChangeNotifier{
  List<Post>? _postsList;

  List<Post>? get postsList => _postsList;

  void fetchPosts() async{
    Dio dio = Dio();
    Response res = await dio.get("https://jsonplaceholder.typicode.com/posts");
    PostsList data = PostsList.fromJson(res.data);
    _postsList = data.data;
    notifyListeners();
  }

  void addPosts(Post post){
    _postsList?.add(post);
    notifyListeners();
  }
  void deletePost(int? id){
    if (id != null) {
  _postsList?.removeWhere((Post post) => post.id == id);
}
    notifyListeners();
  }
}