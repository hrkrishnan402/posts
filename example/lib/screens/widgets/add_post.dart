import 'dart:math';

import 'package:example/model/model.dart';
import 'package:example/services/posts_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddPosts extends StatefulWidget {
  const AddPosts({ Key? key }) : super(key: key);

  @override
  _AddPostsState createState() => _AddPostsState();
}

class _AddPostsState extends State<AddPosts> {
  TextEditingController _titleController = TextEditingController();
  TextEditingController _bodyController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Consumer<PostsProvider>(
      builder: (context, posts, _) {
        return AlertDialog(
          content: Column(children: [
            TextField(
              controller: _titleController,
            ),
            TextField(
              controller: _bodyController,
            ),
            TextButton(onPressed: (){
              context.read<PostsProvider>().addPosts(Post(
                id: Random().nextInt(100000),
                userId: 1,
                body: _bodyController.text,
                title: _titleController.text,
              ));
              Navigator.pop(context);
            }, child: const Text("Add Post"))
          ],),
          
        );
      }
    );
  }
}