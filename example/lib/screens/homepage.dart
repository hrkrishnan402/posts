import 'package:example/model/model.dart';
import 'package:example/screens/widgets/add_post.dart';
import 'package:example/services/posts_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyHomepage extends StatefulWidget {
  const MyHomepage({Key? key}) : super(key: key);

  @override
  _MyHomepageState createState() => _MyHomepageState();
}

class _MyHomepageState extends State<MyHomepage> {
  @override
  Widget build(BuildContext context) {
    return Consumer<PostsProvider>(builder: (context, posts, child) {
      List<Post> postList = posts.postsList ?? [];
      return Scaffold(
        appBar: AppBar(
          title: const Center(child: Text("Posts")),
        ),
        body: ListView.builder(
          itemBuilder: (ctx, index) => ListTile(
            title: Text(postList[index].title ?? ""),
            trailing:
                TextButton(onPressed: () {
                  context.read<PostsProvider>().deletePost(postList[index].id);
                }, child: const Icon(Icons.delete)),
          ),
          itemCount: postList.length,
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return const AddPosts();
              },
            );
          },
          child: const Icon(Icons.add),
        ),
      );
    });
  }

  @override
  void initState() {
    context.read<PostsProvider>().fetchPosts();
    super.initState();
  }
}
